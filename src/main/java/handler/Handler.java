package handler;

import converter.Converter;
import dto.Dto;

public interface Handler {
    Handler handler = null;

    void setNextHandler(Handler nextHandler);

    void setConverter(Converter converter);

    void createRequest(Dto request, Dto response);
}
