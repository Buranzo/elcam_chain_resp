package handler.impl;

import converter.Converter;
import dto.Dto;
import dto.impl.DtoOpenAccountAe;
import handler.Handler;

import java.util.UUID;

public class AeOpenAccount implements Handler {

    private Handler nextHandler;
    private Converter converter;

    @Override
    public void setNextHandler(Handler nextHandler) {
        this.nextHandler = nextHandler;
    }

    @Override
    public void setConverter(Converter converter) {
        this.converter = converter;
    }

    @Override
    public void createRequest(Dto request, Dto response) {
        request = converter.convert(request, response);
        response = externalService(request);
        System.out.println(String.format("Convert: %s", request));
        System.out.println("AE open account");
        if (null != this.nextHandler) {
            this.nextHandler.createRequest(request, response);
        }
    }

    private Dto externalService(Dto requuest) {
        Dto dto = new DtoOpenAccountAe();
        dto.setId(UUID.randomUUID());
        dto.setAccount("resopnse AE account");
        return dto;
    }
}
