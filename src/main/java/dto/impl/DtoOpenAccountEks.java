package dto.impl;

import dto.Dto;
import lombok.Data;

import java.util.UUID;

@Data
public class DtoOpenAccountEks implements Dto {
    private UUID id;
    private String account;
}
