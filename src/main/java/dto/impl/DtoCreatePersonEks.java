package dto.impl;

import dto.Dto;
import lombok.Data;

import java.util.UUID;

@Data
public class DtoCreatePersonEks implements Dto {
    private UUID id;
    private String account;
}
