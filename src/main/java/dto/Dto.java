package dto;

import java.util.UUID;

public interface Dto {

    String getAccount();

    void setAccount(String account);

    UUID getId();

    void setId(UUID id);
}
