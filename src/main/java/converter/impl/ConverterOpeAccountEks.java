package converter.impl;

import converter.Converter;
import dto.Dto;
import dto.impl.DtoOpenAccountEks;

public class ConverterOpeAccountEks implements Converter {
    @Override
    public Dto convert(Dto request, Dto response) {
        Dto dto = new DtoOpenAccountEks();
        dto.setId(request.getId());
        dto.setAccount(response.getAccount());
        return dto;
    }
}
