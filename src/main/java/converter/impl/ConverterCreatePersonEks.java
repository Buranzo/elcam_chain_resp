package converter.impl;

import converter.Converter;
import dto.Dto;
import dto.impl.DtoCreatePersonEks;

public class ConverterCreatePersonEks implements Converter {
    @Override
    public Dto convert(Dto request, Dto response) {
        Dto dto = new DtoCreatePersonEks();
        dto.setId(request.getId());
        dto.setAccount(response.getAccount());
        return dto;
    }
}
