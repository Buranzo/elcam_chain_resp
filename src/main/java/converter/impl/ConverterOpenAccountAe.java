package converter.impl;

import converter.Converter;
import dto.Dto;
import dto.impl.DtoOpenAccountAe;

public class ConverterOpenAccountAe implements Converter {
    @Override
    public Dto convert(Dto request, Dto response) {
        Dto dto = new DtoOpenAccountAe();
        dto.setId(request.getId());
        dto.setAccount(response.getAccount());
        return dto;
    }
}
