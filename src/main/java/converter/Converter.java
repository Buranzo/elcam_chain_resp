package converter;

import dto.Dto;

public interface Converter {

    Dto convert(Dto request, Dto response);
}
