import converter.Converter;
import converter.impl.ConverterCreatePersonEks;
import converter.impl.ConverterOpeAccountEks;
import converter.impl.ConverterOpenAccountAe;
import dto.Dto;
import dto.impl.DtoCreatePersonEks;
import handler.Handler;
import handler.impl.AeOpenAccount;
import handler.impl.EksCreatePerson;
import handler.impl.EksOpenAccount;

import java.util.UUID;

public class Manager {
    public static void main(String[] args) {

        // Создаём экземпляры обработчиков цепочки
        Handler eksCreatePerson = new EksCreatePerson();
        Handler aeOpenAccount = new AeOpenAccount();
        Handler eksOpenAccount = new EksOpenAccount();

        // Создём экземпляры конверторов
        Converter converterCreatePersonEks = new ConverterCreatePersonEks();
        Converter converterOpenAccountAe = new ConverterOpenAccountAe();
        Converter converterOpenAccountEks = new ConverterOpeAccountEks();

        // Задаём соответствие конверторов и обработчиков
        eksCreatePerson.setConverter(converterCreatePersonEks);
        aeOpenAccount.setConverter(converterOpenAccountAe);
        eksOpenAccount.setConverter(converterOpenAccountEks);

        // Задаём последовательность обработчиков цепочки
        eksCreatePerson.setNextHandler(aeOpenAccount);
        aeOpenAccount.setNextHandler(eksOpenAccount);

        // Заполняем начальное DTO
        Dto dto = new DtoCreatePersonEks();
        dto.setId(UUID.randomUUID());
        dto.setAccount("123");

        // Вызываем первого обработчика, остальные будут вызваны автоматически
        eksCreatePerson.createRequest(dto, new DtoCreatePersonEks());
    }
}
